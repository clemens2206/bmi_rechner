/**
 * Clientseitig
 * Funktion zum Überprüfen des Datums.
 * @param elem Das Datum wird der Methode mitgegeben.
 */
function validateMessDate(elem) {
    let today = new Date();
    today.setHours(0,0,0,0);

    let measurementDate = new Date(elem.value);
    measurementDate.setHours(0,0,0,0);

    //Wenn das Messdatum kleiner gleich heutiges Datum ist.
    if(measurementDate <= today){
        //is valid wird zu der classList geaddet und is-invalid wird gelöscht
        elem.classList.add("is-valid");
        elem.classList.remove("is-invalid");
    }else{
        elem.classList.add("is-invalid");
        elem.classList.remove("is-valid");
    }
}
