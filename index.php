<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" crossorigin="anonymous">
    <title>Body-Mass-Index-Rechner</title>

    <script type="text/javascript" src="js/index.js"></script>
</head>

<body class="bg-dark text-white">
<div class="container ">

<h1 class="mt-5 mb-3">Body-Mass-Index-Rechner</h1>

<?php
//einbeziehen der Datei func.inc.php die im Ordner lib liegt
require "lib/func.inc.php";

$name = '';
$messDate = '';
$height = '';
$weight = '';

//Wenn der submit Button gedrückt wurde
if (isset($_POST['submit'])) {
    //Neue Variable $name wird überprüft ob Namen vorhanden, wenn ja übernehmen sonst leer
    $name = isset($_POST['name']) ? $_POST['name'] : '';
    $messDate = isset($_POST['messDate']) ? $_POST['messDate'] : '';
    $height = isset($_POST['height']) ? $_POST['height'] : '';
    $weight = isset($_POST['weight']) ? $_POST['weight'] : '';

//validate Methode wird aufgerufen
    if (validate($name, $messDate, $height, $weight)) {
        //calculateBMI wird aufgerufen
        $bmi = calculateBMI($height, $weight);

        //Hier wird die jeweilige Kategorie ausgegeben.
        if ($bmi <= 18.5) {
            echo "<p class='alert alert-warning'>BMI: " . $bmi . " - Untergewicht</p>";
        } elseif ($bmi >= 18.6 & $bmi <= 24.9) {
            echo "<p class='alert alert-success'>BMI: " . $bmi . " - Normalgewicht</p>";
        } elseif ($bmi >= 25.0 & $bmi <= 29.9) {
            echo "<p class='alert alert-warning'>BMI: " . $bmi . " - Übergewicht</p>";
        } else {
            echo "<p class='alert alert-danger'>BMI: " . $bmi . " - Adipositas</p>";
        }
    } else {
        //Wenn eine falsche Eingabe getätigt wurde wird der Fehler ausgegeben
        echo "<div class='alert alert-danger'> <p>Die eingegebenen Daten sind Fehlerhaft</p> <ul>";
        if (isset($error)) {
            foreach ($error as $key => $value) {
                echo "<li>" . $value . "</li>";
            }
        }
        echo "</ul></div>";
    }
}

?>
<!--Ziel index.php und post zum übermitteln der Daten-->
<form id="form_grade" action="index.php" method="post">
    <!--Eine Zeile einfügen (Haupt-Container)-->
    <div class="row">
        <!--Spaltenbreite = 8 (Trennung des Containers in zwei Spalten)-->
        <div class="col-sm-8">
            <!--Name und Messdatum Container (Zeile)-->
            <div class="row">
                <!--Container für Name-->
                <div class="col-sm-8 mb-3">
                    <label for="name">Name*</label>
                    <!--Wir überprüfen mit isset.., ob eine Fehlervariabel gesetzt ist, wenn ja wird die is-invalid Markierung gesetzt-->
                    <!--htmlspecialchars() formatiert den eingegebenen Namen in die HTML-Codierung um-->
                    <input type="text"
                           name="name"
                           class="form-control <?= isset($error['name']) ? 'is-invalid' : '' ?>"
                           value="<?=htmlspecialchars($name)?>"
                           maxlength="25"
                           required/>
                </div>

                <!--Container für Messdatum -->
                <div class="col-sm mb-3">
                    <label for="messDate">Messdatum*</label>
                    <!--sollte sich das Messdatum ändern wir die Funktion validateMessDate aufgerufen in der Klasse index.js
                           mit this verweisen wir auf die akutelle Eingabe-->
                    <input type="date"
                           name="messDate"
                           value="<?=htmlspecialchars($messDate)?>"
                           class="form-control <?= isset($error['messDate']) ? 'is-invalid' : ''?>"
                           onchange="validateMessDate(this)"/>
                </div>
            </div>

            <!--neuer Zeilen Container für Größe und Gewicht-->
            <div class="row">

                <!--Container für Größe -->
                <div class="col-sm-6 mb-3">
                    <label for="height">Größe (cm)*</label>
                    <input type="number"
                           name="height"
                           class="form-control <?= isset($error['height']) ? 'is-invalid' : ''?>"
                           value="<?=htmlspecialchars($height)?>"
                           max="260"
                           min="40"
                           required/>
                </div>

                <!--Container für Gewicht -->
                <div class="col-sm-6 mb-3">
                    <label for="weight">Gewicht (kg)*</label>
                    <input type="number"
                           name="weight"
                           class="form-control <?= isset($error['weight']) ? 'is-invalid' : ''?>"
                           value="<?=htmlspecialchars($weight)?>"
                           min="5"
                           max="300"
                           required/>
                </div>
            </div>

            <!--Zeilen-Container für Berechnen und Löschen-->
            <div class="row mt-2">
                <!--Container für Berechnen -->
                <div class="col-sm-3 mb-3">
                    <input name="submit" type="submit" class="btn btn-success w-100"  value="Berechnen">
                </div>

                <!--Container für Löschen-->
                <div class="col-sm-3 mb-3">
                    <a href="index.php" class="btn btn-secondary w-100">Löschen</a>
                </div>
            </div>
        </div>

        <!--Container für die Info-->
        <div class="col-sm-4 ">
            <h3>Info zum BMI</h3>
            <p>Unter 18.5 Untergewicht <br>
                18.5 - 24.9 Normal <br>
                25.0 - 29,9 Übergewicht <br>
                30.0 und darüber Adipositias</p>
        </div>
    </div>
  </form>
 </body>
</html>
