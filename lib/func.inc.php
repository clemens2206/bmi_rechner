<?php
//Serverseitig
//gloable Variable
$error = [];

/**
 * Methode zum überprüfung der Eingaben
 * @param $name Name wird mitgegeben
 * @param $messDate Datum wird mitgegeben
 * @param $height Größe wird mitgegeben
 * @param $weight Gewicht wird mitgegeben
 * @return int|string jede aufgerufene Methode bekommt den jeweiligen Parameter mit, überprüft diese und returnt
 */
function validate($name, $messDate, $height, $weight) {
    return validateName($name) & validateMessDate($messDate) & validateHeight($height) & validateWeight($weight);
}

/**
 * Methode zum überprüfen des Namens
 * @param $name Name wird mitgegeben
 * @return bool true oder false
 */
function validateName($name)
{
    global $error;

    if (strlen($name) == 0) {
        $error['name'] = "Name darf nicht leer sein!";
        return false;
    } else if (strlen($name) > 25) {
        $error['name'] = "Name darf nicht mehr als 25 Zeichen haben!";
        return false;
    } else {
        return true;
    }

}

/**
 * Methode zum überprüfen des Messdatums
 * @param $messDate Messdatum wird mitgegeben
 * @return bool true oder false
 */
function validateMessDate($messDate)
{
    global $error;

    try {
        if ($messDate == "") {
            $error['messDate'] = "Datum darf nicht leer sein!";
            return false;
        } else if (new DateTime($messDate) > new DateTime()) {
            $error['messDate'] = "Das Datum darf nicht in der Zukunft liegen!";
            return false;
        } else {
            return true;
        }
    } catch (Exception $exception) {
        $error['messDate'] = "Messdatum ungültig";
        return false;
    }
}

/**
 * Methode zum überprüfen der Größe
 * @param $height Höhe wird mitgegeben
 * @return bool true oder false
 */
    function validateHeight($height)
    {
        global $error;

        if (!is_numeric($height) || $height > 260 || $height < 40) {
            $error['height'] = "Größe ist ungültig.";
            return false;
        } else {
            return true;
        }
    }

/**
 * Methode zum überprüfen des Gewichts
 * @param $weigth Gewicht wird mitgegeben
 * @return bool true oder false
 */
    function validateWeight($weigth)
    {
        global $error;

        if (!is_numeric($weigth) || $weigth > 300 || $weigth < 5) {
            $error['height'] = "Gewicht ist ungültig.";
            return false;
        } else {
            return true;
        }
    }

/**
 * Methode zum berechnen des BMIs
 * @param $height Größe wird mitgegeben
 * @param $weight Gewicht wird mitgegeben
 * @return float bmi wird zurückgegeben.
 */
function calculateBMI($height, $weight){
    $bmi = round($weight/pow($height/100, 2), 1);
    return $bmi;
}

?>
